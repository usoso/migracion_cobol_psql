CREATE OR REPLACE FUNCTion [SCHEMA].proceso() RETURNS VOID AS
$BODY$
	DECLARE
		IntPosicion			integer :=0;
		ClaveTotalColor			varchar(100);
		IntIdLocalidad			bigint :=0;
		IntIdCalle			bigint :=0;
		IntIdDomicilio			bigint :=0;

		reg_paises			RECORD;
		reg_provincias          	RECORD;
		reg_localidades			RECORD;
		reg_tiposDocumentos		RECORD;
		reg_estadosCiviles		RECORD;
		reg_nacionalidades		RECORD;
		reg_profesiones			RECORD;
		reg_sexo			RECORD;
		reg_canalesVentas		RECORD;
		reg_sexoMascotas		RECORD;
		reg_especiesMascotas		RECORD;
		reg_razasMascotas		RECORD;
		reg_color			RECORD;
		reg_mascotas			RECORD;
		reg_personas			RECORD;
		reg_TitularesTcCbu		RECORD;
		reg_tiposZonas			RECORD;
		reg_otrosCatalogos		RECORD;
		reg_categoriasPlanes		RECORD;
		reg_planes			RECORD;
		reg_zonas			RECORD;
		reg_callesDomicilios		RECORD;
		reg_sucursales			RECORD;
		reg_puntosVentas		RECORD;
		reg_usuarios			RECORD;
		reg_cobradores			RECORD;
		reg_zonasEmpleados		RECORD;
		reg_vendedores			RECORD;
		reg_coordinadores		RECORD;
		reg_equiposVendedores	RECORD;
		reg_personasContactos	RECORD;
		reg_VendedoresPorEquipo	RECORD;
		reg_entidadesFinancieras	RECORD;
		reg_tarjetasCredito		RECORD;
		reg_Cbus				RECORD;

		cur_paises CURSOR FOR SELECT p.id, btrim(p.nombre) as nombre,
					(SELECT lpad(p.id::text, largo_clave, '0') FROM public.maestros WHERE alias = 'Paises') as claveTotal,
					(SELECT id FROM public.maestros WHERE alias = 'Paises') as maestro
				FROM [SCHEMA].paises p
				ORDER BY id;

		cur_provincias CURSOR FOR SELECT pr.id, btrim(pr.nombre) as nombre,
					(SELECT lpad(p.id::text, largo_clave, '0') FROM public.maestros WHERE alias = 'Paises')||(SELECT lpad(pr.id::text, largo_clave, '0') FROM public.maestros WHERE alias = 'Provincias') as claveTotal,
					(SELECT id FROM public.maestros WHERE alias = 'Provincias') as maestro
				FROM [SCHEMA].provincias pr
					INNER JOIN [SCHEMA].paises p
						ON pr.idpais = p.id
				ORDER BY pr.id;
				
		cur_localidades CURSOR FOR SELECT l.id, l.codigo_postal, btrim(l.localidad) as localidad,
						(SELECT lpad(p.id::text, largo_clave, '0') FROM public.maestros WHERE alias = 'Paises')||(SELECT lpad(pr.id::text, largo_clave, '0') FROM public.maestros WHERE alias = 'Provincias')||(SELECT lpad(l.id::text, largo_clave, '0') FROM public.maestros WHERE alias = 'Localidades') as claveTotal,
						(SELECT id FROM public.maestros WHERE alias = 'Localidades') as maestro
					FROM [SCHEMA].localidades l
						INNER JOIN [SCHEMA].provincias pr
							ON l.provincia = pr.id
						INNER JOIN [SCHEMA].paises p
							ON pr.idpais = p.id
					ORDER BY l.id;
					
		cur_tiposDocumentos CURSOR FOR SELECT t.id, btrim(t.descripcion_corta) as descripcion_corta,
						(SELECT lpad(t.id::text, largo_clave, '0') FROM public.maestros WHERE alias = 'TiposDocumentos') as claveTotal,
						(SELECT id FROM public.maestros WHERE alias = 'TiposDocumentos') as maestro
					FROM [SCHEMA].tipos_documentos t;

		cur_estadosCiviles CURSOR FOR SELECT e.id, btrim(e.descripcion) as descripcion,
						(SELECT lpad(e.id::text, largo_clave, '0') FROM public.maestros WHERE alias = 'EstadosCiviles') as claveTotal,
						(SELECT id FROM public.maestros WHERE alias = 'EstadosCiviles') as maestro
					FROM [SCHEMA].estados_civiles e;

		cur_nacionalidades CURSOR FOR SELECT n.id, btrim(n.descripcion) as descripcion,
						(SELECT lpad(n.id::text, largo_clave, '0') FROM public.maestros WHERE alias = 'Nacionalidades') as claveTotal,
						(SELECT id FROM public.maestros WHERE alias = 'Nacionalidades') as maestro
					FROM [SCHEMA].nacionalidades n;

		cur_profesiones CURSOR FOR SELECT p.id, btrim(p.descripcion) as descripcion,
						(SELECT lpad(p.id::text, largo_clave, '0') FROM public.maestros WHERE alias = 'Profesiones') as claveTotal,
						(SELECT id FROM public.maestros WHERE alias = 'Profesiones') as maestro
					FROM [SCHEMA].profesiones p;

		cur_sexo CURSOR FOR SELECT s.id, btrim(s.descripcion) as descripcion,
					(SELECT lpad(s.id::text, largo_clave, '0') FROM public.maestros WHERE alias = 'Sexo') as claveTotal,
					(SELECT id FROM public.maestros WHERE alias = 'Sexo') as maestro
				FROM [SCHEMA].sexo s;

		cur_canalesVentas CURSOR FOR SELECT c.id, btrim(c.descripcion) as descripcion,
						(SELECT lpad(c.id::text, largo_clave, '0') FROM public.maestros WHERE alias = 'CanalesVentas') as claveTotal,
						(SELECT id FROM public.maestros WHERE alias = 'CanalesVentas') as maestro
					FROM [SCHEMA].canales_ventas c;

		cur_sexoMascotas CURSOR FOR SELECT s.id, btrim(s.descripcion) as descripcion,
						(SELECT lpad(s.id::text, largo_clave, '0') FROM public.maestros WHERE alias = 'SexoAnimal') as claveTotal,
						(SELECT id FROM public.maestros WHERE alias = 'SexoAnimal') as maestro
					FROM [SCHEMA].sexo_mascotas s;

		cur_especiesMascotas CURSOR FOR SELECT e.codigo, btrim(e.descripcion) as descripcion,
						(SELECT lpad(e.codigo::text, largo_clave, '0') FROM public.maestros WHERE alias = 'Especies') as claveTotal,
						(SELECT id FROM public.maestros WHERE alias = 'Especies') as maestro
					FROM [SCHEMA].especies_mascotas e;

		cur_razasMascotas CURSOR FOR SELECT r.codigo, btrim(r.descripcion) as descripcion,
						(SELECT lpad(r.codigo::text, largo_clave, '0') FROM public.maestros WHERE alias = 'Razas') as claveTotal,
						(SELECT id FROM public.maestros WHERE alias = 'Razas') as maestro
					FROM [SCHEMA].razas_mascotas r;

		cur_color CURSOR FOR SELECT distinct REPLACE(REPLACE(m.color, '/', ''), '-', '') as color,
					(SELECT id FROM public.maestros WHERE alias = 'Color') as maestro
				FROM [SCHEMA].mascotas m;

		cur_personas CURSOR FOR SELECT p.id,
					btrim(p.nombre_comercial) as nombre_comercial,
					td.id as idtipo_documento,
					p.numero_documento as numero_documento,
					p.fecha_nacimiento as fecha_nacimiento,
					ec.id as idestado_civil,
					n.id as idnacionalidad,
					pr.id as idprofesion,
					p.es_fisica as es_fisica,
					s.id as idsexo,
					btrim(p.apellido) as apellido,
					btrim(p.nombre) as nombre,
					regexp_match(btrim(p.cuit),'*', NULL) as cuit,
					p.fecha_alta as fecha_creacion,
					p.fecha_baja as fecha_borrado
				FROM [SCHEMA].personas p
					INNER JOIN public.catalogos td
						ON td.idmaestro = 4 --TipoDocumento
						AND td.clave = p.tipo_documeno::text
					LEFT OUTER JOIN public.catalogos ec
						ON ec.idmaestro = 5 --EstadoCivil
						AND ec.clave = p.estado_civil::text
					LEFT OUTER JOIN public.catalogos n
						ON n.idmaestro = 6 --Nacionalidad
						AND n.clave = p.nacionalidad::text
					LEFT OUTER JOIN public.catalogos pr
						ON pr.idmaestro = 7 --Profesion
						AND pr.clave = p.profesion::text
					LEFT OUTER JOIN public.catalogos s
						ON s.idmaestro = 8 --Sexo
						AND s.clave = p.idsexo::text
				ORDER BY p.id;

		cur_TitularesTcCbu CURSOR FOR SELECT titular_cbu_tc as nombre_comercial, 
						787 as idtipo_documento, 
						mc.ndoc_cbu_tc::text as numero_documento, 
						true as es_fisica, 
						NOW() as fecha_creacion
					FROM [SCHEMA].contratos mc
					WHERE mc.ndoc_cbu_tc = mc.dni_suscriptor
						AND mc.ndoc_cbu_tc <> 0
						AND (numero_tarjeta <> 0 
						OR (btrim(cbu) <> '* *' AND btrim(cbu) <> ''))
						AND mc.ndoc_cbu_tc::text not in (
												SELECT p.numero_documento
												FROM [SCHEMA].contratos c
												INNER JOIN public.personas p
												ON c.ndoc_cbu_tc::text = p.numero_documento
												)
					UNION
					SELECT titular_cbu_tc as nombre_comercial, 
						787 as idtipo_documento, 
						mc.ndoc_cbu_tc::text as numero_documento, 
						true as es_fisica, 
						NOW() as fecha_creacion
					FROM [SCHEMA].contratos mc
					WHERE mc.ndoc_cbu_tc <> mc.dni_suscriptor
						AND mc.ndoc_cbu_tc <> 0
						AND (numero_tarjeta <> 0 
						OR (btrim(cbu) <> '* *' AND btrim(cbu) <> ''))
						AND mc.ndoc_cbu_tc::text not in (
												SELECT p.numero_documento
												FROM [SCHEMA].contratos c
												INNER JOIN public.personas p
												ON c.ndoc_cbu_tc::text = p.numero_documento
												);

		cur_tiposZonas CURSOR FOR SELECT 43 as maestro,
					tz.id as clave,
					btrim(tz.descripcion) as descripcion,
					btrim(tz.descripcion) as descripcion_corta,
					tz.id as claveTotal
				FROM [SCHEMA].tipos_zonas_cobranza tz
				ORDER BY tz.id;

		cur_otrosCatalogos CURSOR FOR SELECT co.idmaestro as maestro, co.clave, co.descripcion, co.descripcion_corta, co.clave_total as claveTotal
					FROM [SCHEMA].catalogo_old co
					WHERE co.idmaestro in (14, 32, 33, 34, 47, 24, 22)
					ORDER BY co.id;

		cur_categoriasPlanes CURSOR FOR SELECT btrim(cp.denominacion) as denominacion, cp.fecha_vigencia, c.id as idempresa
					FROM [SCHEMA].categorias_planes_old cp
						INNER JOIN public.catalogos c
							ON c.idmaestro = 14
							AND c.clave = cp.idempresa::text
					ORDER BY cp.id;

		cur_planes CURSOR FOR SELECT btrim(p.plan) as descripcion,
					ep.id as idestado,
					tp.id as idtipo_producto,
					pa.id as idgrado_parentesco,
					p.promedio_edad as promedio_edad,
					p.maximo_personas_grupo as maximo_personas_grupo,
					p.maximo_personas_adherentes as maximo_personas_adherentes,
					p.edad_individual_desde as edad_individual_desde,
					p.edad_individual_hasta as edad_individual_hasta,
					p.edad_individual_desde_pase as edad_individual_desde_pase,
					p.edad_individual_hasta_pase as edad_individual_hasta_pase,
					p.edad_adherente_desde as edad_adherente_desde,
					p.edad_adherente_hasta as edad_adherente_hasta,
					p.edad_grupo_desde as edad_grupo_desde,
					p.edad_grupo_hasta as edad_grupo_hasta,
					p.fecha_desde as fecha_desde,
					p.fecha_hasta as fecha_hasta,
					p.fecha_modificacion as fecha_modificacion,
					p.permite_pase as permite_pase,
					em.id as idempresa,
					c.id as idconvenio,
					p.fecha_creacion as fecha_creacion,
					p.fecha_borrado as fecha_borrado,
					cb.id as idcalculo_bonificaciones,
					1 as idcategoria
				FROM [SCHEMA].planes p
					INNER JOIN public.catalogos ep
						ON ep.idmaestro = 32  --EstadosPlanes
						AND ep.clave = p.idestado::text
					INNER JOIN public.catalogos tp
						ON tp.idmaestro = 33  --TiposProductos
						AND tp.clave = p.idtipo_producto::text
					LEFT OUTER JOIN public.catalogos pa
						ON pa.idmaestro = 34	--Parentescos
						AND pa.clave = p.idgrado_parentesco::text
					LEFT OUTER JOIN public.catalogos c
						ON c.idmaestro = 41	--Convenios
						AND c.clave = p.idconvenio::text
					LEFT OUTER JOIN public.catalogos cb
						ON cb.idmaestro = 47	--TipoCalculoBonificaciones
						AND cb.clave = p.idcalculo_bonificaciones::text
					LEFT OUTER JOIN public.catalogos em
						ON em.idmaestro = 14	--Empresas
						AND em.clave = p.idempresa::text
				ORDER BY p.id;

		cur_zonas CURSOR FOR SELECT tz.id as idtipo_zona,
					btrim(z.denominacion) as denominacion,
					btrim(z.descriptor) as descripcion,
					e.id as idempresa,
					el.idcatalogo as idlocalidad,
					z.fecha_alta as fecha_creacion,
					z.fecha_borrado
				FROM [SCHEMA].zonas z
					INNER JOIN public.catalogos tz
						ON tz.idmaestro = 43 --TiposZonas
						AND tz.clave = z.tipo_zona::text
					INNER JOIN public.catalogos e
						ON e.idmaestro = 14 --Empresas
						AND e.clave = z.idempresa::text
					INNER JOIN especificos_localidades el
						ON z.idlocalidad = el.codigo_postal
				ORDER BY z.id;

		cur_callesDomicilios CURSOR FOR SELECT ca.id, btrim(ca.nombre) as nombre,
					(SELECT lpad(p.id::text, largo_clave, '0') FROM public.maestros WHERE alias = 'Paises')||(SELECT lpad(pr.id::text, largo_clave, '0') FROM public.maestros WHERE alias = 'Provincias')||(SELECT lpad(l.id::text, largo_clave, '0') FROM public.maestros WHERE alias = 'Localidades')||(SELECT lpad(ca.id::text, largo_clave, '0') FROM public.maestros WHERE alias = 'Calles') as claveTotal,
					(SELECT id FROM public.maestros WHERE alias = 'Calles') as maestro,
					d.altura,
					d.bis,
					btrim(d.letra) as letra,
					btrim(d.resto) as resto,
					true as seleccion_manual
				FROM [SCHEMA].calles ca
					INNER JOIN [SCHEMA].localidades l
						ON btrim(ca.localidad, ' ') = btrim(l.localidad, ' ')
					INNER JOIN [SCHEMA].provincias pr
						ON l.provincia = pr.id
					INNER JOIN [SCHEMA].paises p
						ON pr.idpais = p.id
					INNER JOIN [SCHEMA].domicilios d
						ON d.idcalle = ca.id
				ORDER BY ca.id;

		cur_sucursales CURSOR FOR SELECT c.id as idCalle,
					d.id as idAltura,
					CASE
						WHEN ca.id IS NULL THEN '0'
						ELSE ca.id
					END as clave,
					CASE
						WHEN c.id IS NULL THEN (SELECT lpad(p.id::text, largo_clave, '0') FROM public.maestros WHERE alias = 'Paises')||(SELECT lpad(pr.id::text, largo_clave, '0') FROM public.maestros WHERE alias = 'Provincias')||(SELECT lpad(l.id::text, largo_clave, '0') FROM public.maestros WHERE alias = 'Localidades')
						ELSE (SELECT lpad(p.id::text, largo_clave, '0') FROM public.maestros WHERE alias = 'Paises')||(SELECT lpad(pr.id::text, largo_clave, '0') FROM public.maestros WHERE alias = 'Provincias')||(SELECT lpad(l.id::text, largo_clave, '0') FROM public.maestros WHERE alias = 'Localidades')||(SELECT lpad(c.id::text, largo_clave, '0') FROM public.maestros WHERE alias = 'Calles')
					END as claveTotal,
					(SELECT id FROM public.maestros WHERE alias = 'Calles') as maestro,
					s.localidad,
					s.nombre,
					s.denominacion,
					btrim(btrim(s.domicilio,SUBSTRING(s.domicilio, 'X*([0-9]{1,9})'))) as calle,
					SUBSTRING(s.domicilio, 'X*([0-9]{1,9})')::integer as altura,
					s.telefono,
					s.fecha_alta,
					s.codigo_sucursal
				FROM [SCHEMA].sucursales s
					INNER JOIN [SCHEMA].localidades l
						ON btrim(regexp_replace(regexp_replace(s.localidad,'VILLA','V.'),'GOBERNADOR','G.')) = btrim(l.localidad)
						OR replace(regexp_replace(regexp_replace(s.localidad,'VILLA','V.'),'GOBERNADOR','G.'), ' ', '') = btrim(l.localidad)
					INNER JOIN [SCHEMA].provincias pr
						ON l.provincia = pr.id
					INNER JOIN [SCHEMA].paises p
						ON pr.idpais = p.id
					LEFT OUTER JOIN [SCHEMA].calles ca
						ON btrim(ca.nombre) = btrim(btrim(s.domicilio,SUBSTRING(s.domicilio, 'X*([0-9]{1,9})')))
					LEFT OUTER JOIN public.catalogos c
						ON c.idmaestro = 25
						and btrim(c.descripcion) = btrim(btrim(s.domicilio,SUBSTRING(s.domicilio, 'X*([0-9]{1,9})')))
					LEFT OUTER JOIN public.domicilios d
						ON c.id = d.idcalle
						AND d.altura = SUBSTRING(s.domicilio, 'X*([0-9]{1,9})')::integer
				ORDER BY s.codigo_sucursal;
				
		cur_puntosVentas CURSOR FOR SELECT btrim(pv.denominacion) as denominacion,
						pv.cuenta_corriente,
						pv.fecha_creacion,
						pv.numero,
						ps.id as idSucursal,
						pv.manual
					FROM [SCHEMA].puntos_ventas pv
					INNER JOIN [SCHEMA].sucursales s
					ON pv.codigo_sucursal = s.codigo_sucursal
					INNER JOIN public.sucursales ps
					ON s.codigo_sucursal = ps.codigo;

		cur_usuarios CURSOR FOR SELECT p.id as idPersona, 
						btrim(u.usuario) as usuario, 
						btrim(u.clave_acceso) as claveAcceso, 
						COALESCE(u.fecha_alta, NOW()) as fechaCreacion, 
						u.fecha_baja as fechaBorrado, 
						u.es_super_usuario as esSuperUsuario
					FROM [SCHEMA].usuarios u
						INNER JOIN public.personas p
						ON u.numero_documento::text = p.numero_documento
					ORDER BY u.id, u.fecha_baja DESC;

		cur_cobradores CURSOR FOR SELECT pp.id as idPersona,
					 mc.liquida_comision as liquidaComision,
					 btrim(mc.observaciones) as observaciones,
					 mc.fecha_alta as fechaAlta,
					 mc.fecha_baja as fechaBaja,
					 mc.idempresa as idEmpresa,
					 btrim(mc.motivo_baja) as motivoBaja,
					 mc.numero_legajo as numeroLegajo,
					 mc.fecha_creacion as fechaCreacion,
					 mc.fecha_borrado as fechaBorrado,
					 mc.baja_definitiva as bajaDefinitiva
				FROM [SCHEMA].cobradores mc
					INNER JOIN public.personas pp
						ON mc.numero_documento::text = pp.numero_documento
				ORDER BY pp.id, fecha_baja desc;
				
		cur_zonasEmpleados CURSOR FOR SELECT COALESCE(fecha_desde, NOW()) as fechaDesde,
					pz.id as idZona,
					pc.id as idCobrador
				FROM [SCHEMA].zonas_empleados ze
					INNER JOIN [SCHEMA].zonas z
						ON ze.zona = z.id
					INNER JOIN public.zonas pz
						ON btrim(z.denominacion) = btrim(pz.denominacion)
					INNER JOIN [SCHEMA].cobradores c
						ON ze.idcobrador = c.id
					INNER JOIN public.personas p
						ON c.numero_documento::text = p.numero_documento
					INNER JOIN public.cobradores pc
						ON p.id = pc.idpersona
				ORDER BY ze.id;
				
		cur_vendedores CURSOR FOR SELECT p.id as idPersona,
					v.liquida_comision as liquidaComision,
					v.recuperador,
					btrim(v.ovservaciones) as observaciones,
					v.codigo_equipo as codigoEquipo,
					v.codigo_coordinador as codigoCoordinador,
					v.posicion_ranking_manual as posicionRankingManual,
					v.fecha_alta as fechaAlta,
					v.fecha_baja as fechaBaja,
					v.baja_definitiva as bajaDefinitiva,
					btrim(v.motivo_baja) as motivoBaja,
					e.id as idEmpresa,
					v.numero_legajo as numeroLegajo,
					v.posicion_ranking_recupero as posicionRankingRecupero,
					v.fecha_creacion as fechaCreacion,
					v.fecha_borrado as fechaBorrado
				FROM [SCHEMA].vendedores v
					INNER JOIN public.personas p
						ON v.numero_documento::text = p.numero_documento
					INNER JOIN public.catalogos e
						ON e.idmaestro = (SELECT id FROM public.maestros WHERE alias = 'Empresas')
						AND v.idempresa::text = e.clave
				ORDER BY v.id;
				
		cur_coordinadores CURSOR FOR SELECT c.codigo_coordinador as codigoCoordinador,
					c.fecha_alta as fechaAlta,
					c.fecha_baja as fechaBaja,
					btrim(c.motivo_baja) as motivoBaja,
					pv.id as idVendedor,
					c.liquida_comision as liquidaComision
				FROM [SCHEMA].coordinadores c
					INNER JOIN [SCHEMA].vendedores v
						ON c.codigo_vendedor = v.codigo_vendedor
					INNER JOIN public.personas p
						ON v.numero_documento::text = p.numero_documento
					INNER JOIN public.vendedores pv
						ON pv.idpersona = p.id
				ORDER BY c.codigo_coordinador;
		
		cur_equiposVendedores CURSOR FOR SELECT ev.codigo_equipo as codigoEquipo,
					pv.id as idVendedor,
					ev.fecha_alta as fechaAlta,
					ev.fecha_baja as fechaBaja,
					btrim(ev.motivo_baja) as motivoBaja,
					ev.liquida_comision as liquidaComision
				FROM [SCHEMA].vendedores_equipos ev
					INNER JOIN [SCHEMA].vendedores v
						ON ev.codigo_vendedor = v.codigo_vendedor
					INNER JOIN public.personas p
						ON v.numero_documento::text = p.numero_documento
					INNER JOIN public.vendedores pv
						ON pv.idpersona = p.id
				ORDER BY ev.codigo_equipo;
		
		cur_personasContactos CURSOR FOR SELECT
					pp.id as idPersona,
					ca.id as idTipoContacto,
					btrim(pc.valor) as valor,
					true as activo
				FROM [SCHEMA].personas_contactos pc
					INNER JOIN [SCHEMA].contratos c
						ON pc.numero_socio = c.numero_socio
					INNER JOIN [SCHEMA].personas p
						ON c.dni_suscriptor = p.numero_documento
					INNER JOIN public.catalogos ca
						ON ca.idmaestro = (SELECT id FROM public.maestros where alias = 'TiposContactos')
						AND btrim(upper(replace(pc.idtipo_contacto, '-',''))) like btrim(upper(replace(ca.descripcion, '-','')))||'%'
					INNER JOIN public.personas pp
						ON c.dni_suscriptor::text = pp.numero_documento
				union
				SELECT 
					pp.id as idPersona,
					ca.id as idTipoContacto,
					btrim(pc.valor) as valor,
					true as activo
				FROM [SCHEMA].personas_contactos pc
					INNER JOIN [SCHEMA].contratos_personas c
						ON pc.numero_socio = c.numero_contrato
					INNER JOIN [SCHEMA].personas p
						ON c.numero_documento = p.numero_documento
					INNER JOIN public.catalogos ca
						ON ca.idmaestro = (SELECT id FROM public.maestros where alias = 'TiposContactos')
						AND btrim(upper(replace(pc.idtipo_contacto, '-',''))) like btrim(upper(replace(ca.descripcion, '-','')))||'%'
					INNER JOIN public.personas pp
						ON c.numero_documento::text = pp.numero_documento
				ORDER BY 1;
				
		cur_VendedoresPorEquipo CURSOR FOR SELECT 
						pv.id as idVendedor, 
						ev.id as idEquipo,
						c.id as idCoordinador
					FROM [SCHEMA].vendedores v
						INNER JOIN public.personas p
							ON v.numero_documento::text = p.numero_documento
						INNER JOIN public.vendedores pv
							ON pv.idpersona = p.id
						LEFT OUTER JOIN public.coordinadores c
							ON v.codigo_coordinador = c.codigo_coordinador
						LEFT OUTER JOIN public.equipos_vendedores ev
							ON v.codigo_equipo = ev.codigo_equipo
					ORDER BY pv.id;
		
		cur_EntidadesFinancieras CURSOR FOR SELECT DISTINCT REPLACE(BTRIM(tarjeta_banco), 'C.B.U. ', '') as nombre, NOW() as fechaAlta
					FROM [SCHEMA].contratos
					WHERE numero_tarjeta <> 0 
					OR (btrim(cbu) <> '* *' AND btrim(cbu) <> '');
		
		cur_tarjetasCredito CURSOR FOR SELECT numero_tarjeta, 
						p.id as idTitular, 
						ef.id as idEntidadFinanciera, 
						ef.nombre
					FROM [SCHEMA].contratos mc
						INNER JOIN public.personas p
							ON mc.ndoc_cbu_tc::text = p.numero_documento
						INNER JOIN public.entidades_financieras ef
							ON REPLACE(BTRIM(tarjeta_banco), 'C.B.U. ', '') = ef.nombre
					WHERE mc.ndoc_cbu_tc = mc.dni_suscriptor
						AND mc.ndoc_cbu_tc <> 0
						AND numero_tarjeta <> 0 
					union
					SELECT numero_tarjeta, 
						p.id as idTitular, 
						ef.id as idEntidadFinanciera, 
						ef.nombre
					FROM [SCHEMA].contratos mc
						INNER JOIN public.personas p
							ON mc.ndoc_cbu_tc::text = p.numero_documento
						INNER JOIN public.entidades_financieras ef
							ON REPLACE(BTRIM(tarjeta_banco), 'C.B.U. ', '') = ef.nombre
					WHERE mc.ndoc_cbu_tc <> mc.dni_suscriptor
						AND mc.ndoc_cbu_tc <> 0
						AND numero_tarjeta <> 0;
	
		cur_Cbus CURSOR FOR SELECT btrim(cbu) as cbu, 
						p.id as idTitular, 
						ef.id as idEntidadFinanciera,
						ef.nombre
					FROM [SCHEMA].contratos mc
						INNER JOIN public.personas p
						ON mc.ndoc_cbu_tc::text = p.numero_documento
						INNER JOIN public.entidades_financieras ef
						ON REPLACE(BTRIM(tarjeta_banco), 'C.B.U. ', '') = ef.nombre
					WHERE mc.ndoc_cbu_tc = mc.dni_suscriptor
						AND mc.ndoc_cbu_tc <> 0
						AND (btrim(cbu) <> '* *' AND btrim(cbu) <> '')
					union
					SELECT btrim(cbu) as cbu, 
						p.id as idTitular, 
						ef.id as idEntidadFinanciera,
						ef.nombre
					FROM [SCHEMA].contratos mc
						INNER JOIN public.personas p
						ON mc.ndoc_cbu_tc::text = p.numero_documento
						INNER JOIN public.entidades_financieras ef
						ON REPLACE(BTRIM(tarjeta_banco), 'C.B.U. ', '') = ef.nombre
					WHERE mc.ndoc_cbu_tc <> mc.dni_suscriptor
						AND mc.ndoc_cbu_tc <> 0
						AND (btrim(cbu) <> '* *' AND btrim(cbu) <> '');


	BEGIN
		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  Paises';
		RAISE NOTICE '----------------------';
		FOR reg_paises IN cur_paises LOOP
			RAISE NOTICE ' PROCESANDO  %', reg_paises.nombre;
			RAISE NOTICE ' CLAVETOTAL  %', reg_paises.claveTotal;
			RAISE NOTICE ' MAESTRO  %', reg_paises.maestro;

			INSERT INTO public.catalogos
			(idmaestro, clave, descripcion, descripcion_corta, fecha_creacion, clave_total)
			SELECT reg_paises.maestro, reg_paises.id, reg_paises.nombre, reg_paises.nombre, now(), reg_paises.claveTotal
			WHERE
				NOT EXISTS (
					SELECT id FROM public.catalogos WHERE descripcion = reg_paises.nombre
				);
		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  Provincias';
		RAISE NOTICE '-----------------------';
		FOR reg_provincias IN cur_provincias LOOP
			RAISE NOTICE ' PROCESANDO  %', reg_provincias.nombre;
			RAISE NOTICE ' CLAVETOTAL  %', reg_provincias.claveTotal;
			RAISE NOTICE ' MAESTRO  %', reg_provincias.maestro;

			INSERT INTO public.catalogos
			(idmaestro, clave, descripcion, descripcion_corta, fecha_creacion, clave_total)
			SELECT reg_provincias.maestro, reg_provincias.id, reg_provincias.nombre, reg_provincias.nombre, now(), reg_provincias.claveTotal
			WHERE
				NOT EXISTS (
					SELECT id FROM public.catalogos WHERE descripcion = reg_provincias.nombre
				);
		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  Localidades';
		RAISE NOTICE '------------------------';
		FOR reg_localidades IN cur_localidades LOOP
			RAISE NOTICE ' PROCESANDO  %', reg_localidades.localidad;
			RAISE NOTICE ' CLAVETOTAL  %', reg_localidades.claveTotal;
			RAISE NOTICE ' MAESTRO  %', reg_localidades.maestro;

			INSERT INTO public.catalogos
			(idmaestro, clave, descripcion, descripcion_corta, fecha_creacion, clave_total)
			SELECT reg_localidades.maestro, reg_localidades.id, reg_localidades.localidad, reg_localidades.localidad, now(), reg_localidades.claveTotal
			WHERE
				NOT EXISTS (
					SELECT id FROM public.catalogos WHERE descripcion = reg_localidades.localidad
				);

			IntIdLocalidad := (SELECT id FROM public.catalogos WHERE idmaestro = (SELECT id FROM public.maestros WHERE alias = 'Localidades') AND descripcion = reg_localidades.localidad);

			INSERT INTO public.especificos_localidades
			(idcatalogo, codigo_postal)
			SELECT IntIdLocalidad, reg_localidades.codigo_postal
			WHERE
				NOT EXISTS (
					SELECT idcatalogo FROM public.especificos_localidades WHERE codigo_postal = reg_localidades.codigo_postal
				);
		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  Tipos Documentos';
		RAISE NOTICE '-----------------------------';
		FOR reg_tiposDocumentos IN cur_tiposDocumentos LOOP

			RAISE NOTICE ' PROCESANDO  %', reg_tiposDocumentos.descripcion_corta;
			RAISE NOTICE ' CLAVETOTAL  %', reg_tiposDocumentos.claveTotal;
			RAISE NOTICE ' MAESTRO  %', reg_tiposDocumentos.maestro;

			INSERT INTO public.catalogos
			(idmaestro, clave, descripcion, descripcion_corta, fecha_creacion, clave_total)
			SELECT reg_tiposDocumentos.maestro, reg_tiposDocumentos.id, reg_tiposDocumentos.descripcion_corta, reg_tiposDocumentos.descripcion_corta, now(), reg_tiposDocumentos.claveTotal
			WHERE
				NOT EXISTS (
					SELECT id FROM public.catalogos WHERE descripcion = reg_tiposDocumentos.descripcion_corta
				);

		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  Estados Civiles';
		RAISE NOTICE '----------------------------';
		FOR reg_estadosCiviles IN cur_estadosCiviles LOOP

			RAISE NOTICE ' PROCESANDO  %', reg_estadosCiviles.descripcion;
			RAISE NOTICE ' CLAVETOTAL  %', reg_estadosCiviles.claveTotal;
			RAISE NOTICE ' MAESTRO  %', reg_estadosCiviles.maestro;

			INSERT INTO public.catalogos
			(idmaestro, clave, descripcion, descripcion_corta, fecha_creacion, clave_total)
			SELECT reg_estadosCiviles.maestro, reg_estadosCiviles.id, reg_estadosCiviles.descripcion, reg_estadosCiviles.descripcion, now(), reg_estadosCiviles.claveTotal
			WHERE
				NOT EXISTS (
					SELECT id FROM public.catalogos WHERE descripcion = reg_estadosCiviles.descripcion
				);

		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  Nacionalidades';
		RAISE NOTICE '---------------------------';
		FOR reg_nacionalidades IN cur_nacionalidades LOOP

			RAISE NOTICE ' PROCESANDO  %', reg_nacionalidades.descripcion;
			RAISE NOTICE ' CLAVETOTAL  %', reg_nacionalidades.claveTotal;
			RAISE NOTICE ' MAESTRO  %', reg_nacionalidades.maestro;

			INSERT INTO public.catalogos
			(idmaestro, clave, descripcion, descripcion_corta, fecha_creacion, clave_total)
			SELECT reg_nacionalidades.maestro, reg_nacionalidades.id, reg_nacionalidades.descripcion, reg_nacionalidades.descripcion, now(), reg_nacionalidades.claveTotal
			WHERE
				NOT EXISTS (
					SELECT id FROM public.catalogos WHERE descripcion = reg_nacionalidades.descripcion
				);

		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  Profesiones';
		RAISE NOTICE '------------------------';
		FOR reg_profesiones IN cur_profesiones LOOP

			RAISE NOTICE ' PROCESANDO  %', reg_profesiones.descripcion;
			RAISE NOTICE ' CLAVETOTAL  %', reg_profesiones.claveTotal;
			RAISE NOTICE ' MAESTRO  %', reg_profesiones.maestro;

			INSERT INTO public.catalogos
			(idmaestro, clave, descripcion, descripcion_corta, fecha_creacion, clave_total)
			SELECT reg_profesiones.maestro, reg_profesiones.id, reg_profesiones.descripcion, reg_profesiones.descripcion, now(), reg_profesiones.claveTotal
			WHERE
				NOT EXISTS (
					SELECT id FROM public.catalogos WHERE descripcion = reg_profesiones.descripcion
				);

		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  SEXO';
		RAISE NOTICE '-----------------';
		FOR reg_sexo IN cur_sexo LOOP

			RAISE NOTICE ' PROCESANDO  %', reg_sexo.descripcion;
			RAISE NOTICE ' CLAVETOTAL  %', reg_sexo.claveTotal;
			RAISE NOTICE ' MAESTRO  %', reg_sexo.maestro;

			INSERT INTO public.catalogos
			(idmaestro, clave, descripcion, descripcion_corta, fecha_creacion, clave_total)
			SELECT reg_sexo.maestro, reg_sexo.id, reg_sexo.descripcion, reg_sexo.descripcion, now(), reg_sexo.claveTotal
			WHERE
				NOT EXISTS (
					SELECT id FROM public.catalogos WHERE descripcion = reg_sexo.descripcion
				);

		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  CANALES DE VENTAS';
		RAISE NOTICE '------------------------------';
		FOR reg_canalesVentas IN cur_canalesVentas LOOP

			RAISE NOTICE ' PROCESANDO  %', reg_canalesVentas.descripcion;
			RAISE NOTICE ' CLAVETOTAL  %', reg_canalesVentas.claveTotal;
			RAISE NOTICE ' MAESTRO  %', reg_canalesVentas.maestro;

			INSERT INTO public.catalogos
			(idmaestro, clave, descripcion, descripcion_corta, fecha_creacion, clave_total)
			SELECT reg_canalesVentas.maestro, reg_canalesVentas.id, reg_canalesVentas.descripcion, reg_canalesVentas.descripcion, now(), reg_canalesVentas.claveTotal
			WHERE
				NOT EXISTS (
					SELECT id FROM public.catalogos WHERE descripcion = reg_canalesVentas.descripcion
				);

		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  SEXO MASCOTAS';
		RAISE NOTICE '--------------------------';
		FOR reg_sexoMascotas IN cur_sexoMascotas LOOP

			RAISE NOTICE ' PROCESANDO  %', reg_sexoMascotas.descripcion;
			RAISE NOTICE ' CLAVETOTAL  %', reg_sexoMascotas.claveTotal;
			RAISE NOTICE ' MAESTRO  %', reg_sexoMascotas.maestro;

			INSERT INTO public.catalogos
			(idmaestro, clave, descripcion, descripcion_corta, fecha_creacion, clave_total)
			SELECT reg_sexoMascotas.maestro, reg_sexoMascotas.id, reg_sexoMascotas.descripcion, reg_sexoMascotas.descripcion, now(), reg_sexoMascotas.claveTotal
			WHERE
				NOT EXISTS (
					SELECT id FROM public.catalogos WHERE descripcion = reg_sexoMascotas.descripcion
				);

		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  ESPECIES MASCOTAS';
		RAISE NOTICE '------------------------------';
		FOR reg_especiesMascotas IN cur_especiesMascotas LOOP

			RAISE NOTICE ' PROCESANDO  %', reg_especiesMascotas.descripcion;
			RAISE NOTICE ' CLAVETOTAL  %', reg_especiesMascotas.claveTotal;
			RAISE NOTICE ' MAESTRO  %', reg_especiesMascotas.maestro;

			INSERT INTO public.catalogos
			(idmaestro, clave, descripcion, descripcion_corta, fecha_creacion, clave_total)
			SELECT reg_especiesMascotas.maestro, reg_especiesMascotas.codigo, reg_especiesMascotas.descripcion, reg_especiesMascotas.descripcion, now(), reg_especiesMascotas.claveTotal
			WHERE
				NOT EXISTS (
					SELECT id FROM public.catalogos WHERE descripcion = reg_especiesMascotas.descripcion
				);

		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  RAZAS MASCOTAS';
		RAISE NOTICE '------------------------------';
		FOR reg_razasMascotas IN cur_razasMascotas LOOP

			RAISE NOTICE ' PROCESANDO  %', reg_razasMascotas.descripcion;
			RAISE NOTICE ' CLAVETOTAL  %', reg_razasMascotas.claveTotal;
			RAISE NOTICE ' MAESTRO  %', reg_razasMascotas.maestro;

			INSERT INTO public.catalogos
			(idmaestro, clave, descripcion, descripcion_corta, fecha_creacion, clave_total)
			SELECT reg_razasMascotas.maestro, reg_razasMascotas.codigo, reg_razasMascotas.descripcion, reg_razasMascotas.descripcion, now(), reg_razasMascotas.claveTotal
			WHERE
				NOT EXISTS (
					SELECT id FROM public.catalogos WHERE descripcion = reg_razasMascotas.descripcion
				);

		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  COLOR MASCOTAS';
		RAISE NOTICE '---------------------------';
		FOR reg_color IN cur_color LOOP

			IntPosicion :=IntPosicion+1;
			ClaveTotalColor:= (SELECT lpad(IntPosicion::text, largo_clave, '0') FROM public.maestros WHERE alias = 'Color');

			RAISE NOTICE ' PROCESANDO  %', reg_color.color;
			RAISE NOTICE ' CLAVETOTAL  %', ClaveTotalColor;
			RAISE NOTICE ' MAESTRO  %', reg_color.maestro;

			INSERT INTO public.catalogos
			(idmaestro, clave, descripcion, descripcion_corta, fecha_creacion, clave_total)
			SELECT reg_color.maestro, IntPosicion::text, reg_color.color, reg_color.color, now(), ClaveTotalColor
			WHERE
				NOT EXISTS (
					SELECT id FROM public.catalogos WHERE descripcion = reg_color.color
				);

		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  PERSONAS';
		RAISE NOTICE '---------------------';
			RAISE NOTICE ' PROCESANDO  PERSONA VENTAS OFICINA';

			INSERT INTO public.personas(
				nombre_comercial, idtipo_documento, es_fisica, fecha_creacion)
				VALUES ('Ventas Oficina', 802, false, NOW());

			RAISE NOTICE ' PROCESANDO  PERSONA VENTAS CONVENIOS';

			INSERT INTO public.personas(
				nombre_comercial, idtipo_documento, es_fisica, fecha_creacion)
				VALUES ('Ventas Convenios', 802, false, NOW());
		
		FOR reg_personas IN cur_personas LOOP

			RAISE NOTICE ' PROCESANDO  %', reg_personas.id;
			RAISE NOTICE ' NOMBRE COMERCIAL  %', reg_personas.nombre_comercial;

			INSERT INTO public.personas
			(nombre_comercial, idtipo_documento, numero_documento, fecha_nacimiento, idestado_civil, idnacionalidad, idprofesion, es_fisica, idsexo, apellido, nombre, cuit, fecha_creacion, fecha_borrado)
			SELECT reg_personas.nombre_comercial, reg_personas.idtipo_documento, reg_personas.numero_documento,
					reg_personas.fecha_nacimiento, reg_personas.idestado_civil, reg_personas.idnacionalidad,
					reg_personas.idprofesion, reg_personas.es_fisica, reg_personas.idsexo, reg_personas.apellido,
					reg_personas.nombre, reg_personas.cuit, COALESCE(reg_personas.fecha_creacion, NOW()), reg_personas.fecha_borrado
                        WHERE
                                NOT EXISTS (
                                        SELECT id FROM public.personas WHERE idtipo_documento = reg_personas.idtipo_documento and numero_documento = reg_personas.numero_documento::text
                                );
		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  TITULARES DE TC / CBU';
		RAISE NOTICE '----------------------------------';
		
		FOR reg_TitularesTcCbu IN cur_TitularesTcCbu LOOP

			RAISE NOTICE ' PROCESANDO  %', reg_TitularesTcCbu.numero_documento;
			RAISE NOTICE ' NOMBRE COMERCIAL  %', reg_TitularesTcCbu.nombre_comercial;

			INSERT INTO public.personas
			(nombre_comercial, idtipo_documento, numero_documento, es_fisica, fecha_creacion)
			SELECT reg_TitularesTcCbu.nombre_comercial, reg_TitularesTcCbu.idtipo_documento, reg_TitularesTcCbu.numero_documento,
					reg_TitularesTcCbu.es_fisica, COALESCE(reg_TitularesTcCbu.fecha_creacion, NOW())
                        WHERE
                                NOT EXISTS (
                                        SELECT id FROM public.personas WHERE idtipo_documento = reg_TitularesTcCbu.idtipo_documento and numero_documento = reg_TitularesTcCbu.numero_documento::text
                                );
		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  TIPOS DE ZONAS';
		RAISE NOTICE '---------------------------';
		FOR reg_tiposZonas IN cur_tiposZonas LOOP

			RAISE NOTICE ' PROCESANDO  %', reg_tiposZonas.descripcion;
			RAISE NOTICE ' CLAVETOTAL  %', reg_tiposZonas.claveTotal;
			RAISE NOTICE ' MAESTRO  %', reg_tiposZonas.maestro;

			INSERT INTO public.catalogos
			(idmaestro, clave, descripcion, descripcion_corta, fecha_creacion, clave_total)
			SELECT reg_tiposZonas.maestro, reg_tiposZonas.clave, reg_tiposZonas.descripcion, reg_tiposZonas.descripcion_corta, now(), reg_tiposZonas.claveTotal
			WHERE
				NOT EXISTS (
					SELECT id FROM public.catalogos WHERE descripcion = reg_tiposZonas.descripcion
				);
		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  OTROS CATALOGOS';
		RAISE NOTICE '----------------------------';
		FOR reg_otrosCatalogos IN cur_otrosCatalogos LOOP

			RAISE NOTICE ' PROCESANDO  %', reg_otrosCatalogos.descripcion;
			RAISE NOTICE ' CLAVETOTAL  %', reg_otrosCatalogos.claveTotal;
			RAISE NOTICE ' MAESTRO  %', reg_otrosCatalogos.maestro;

			INSERT INTO public.catalogos
			(idmaestro, clave, descripcion, descripcion_corta, fecha_creacion, clave_total)
			SELECT reg_otrosCatalogos.maestro, reg_otrosCatalogos.clave, reg_otrosCatalogos.descripcion, reg_otrosCatalogos.descripcion_corta, now(), reg_otrosCatalogos.claveTotal
			WHERE
				NOT EXISTS (
					SELECT id FROM public.catalogos WHERE descripcion = reg_otrosCatalogos.descripcion
				);
		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  CATEGORIAS DE PLANES';
		RAISE NOTICE '---------------------------------';
		FOR reg_categoriasPlanes IN cur_categoriasPlanes LOOP

			RAISE NOTICE ' PROCESANDO  %', reg_categoriasPlanes.denominacion;
			RAISE NOTICE ' CLAVETOTAL  %', reg_categoriasPlanes.idempresa;

			INSERT INTO public.categorias_planes
			(denominacion, fecha_vigencia, idempresa)
			SELECT reg_categoriasPlanes.denominacion, reg_categoriasPlanes.fecha_vigencia, reg_categoriasPlanes.idempresa
			WHERE
				NOT EXISTS (
					SELECT id FROM public.categorias_planes WHERE denominacion = reg_categoriasPlanes.denominacion
				);
		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  PLANES';
		RAISE NOTICE '-------------------';
		FOR reg_planes IN cur_planes LOOP

			RAISE NOTICE ' PROCESANDO  %', reg_planes.descripcion;

			INSERT INTO public.planes(
				descripcion, idestado, idtipo_producto, idgrado_parentesco, promedio_edad, maximo_personas_grupo, 
				maximo_personas_adherentes, edad_individual_desde, edad_individual_hasta, edad_individual_desde_pase, 
				edad_individual_hasta_pase, edad_adherente_desde, edad_adherente_hasta, edad_grupo_desde, edad_grupo_hasta, 
				fecha_desde, fecha_hasta, fecha_modificacion, permite_pase, idempresa, idconvenio, fecha_creacion, 
				fecha_borrado, idcalculo_bonificaciones, idcategoria)
				SELECT reg_planes.descripcion, reg_planes.idestado, reg_planes.idtipo_producto, 
						reg_planes.idgrado_parentesco, reg_planes.promedio_edad, reg_planes.maximo_personas_grupo, 
						reg_planes.maximo_personas_adherentes, reg_planes.edad_individual_desde, 
						reg_planes.edad_individual_hasta, reg_planes.edad_individual_desde_pase, 
						reg_planes.edad_individual_hasta_pase, reg_planes.edad_adherente_desde, 
						reg_planes.edad_adherente_hasta, reg_planes.edad_grupo_desde, reg_planes.edad_grupo_hasta, 
						COALESCE(reg_planes.fecha_desde, NOW()), reg_planes.fecha_hasta, reg_planes.fecha_modificacion, 
						COALESCE(reg_planes.permite_pase, false), COALESCE(reg_planes.idempresa,1), reg_planes.idconvenio, 
						COALESCE(reg_planes.fecha_creacion, NOW()), reg_planes.fecha_borrado, 
						reg_planes.idcalculo_bonificaciones, reg_planes.idcategoria
			WHERE
				NOT EXISTS (
					SELECT id FROM public.planes WHERE descripcion = reg_planes.descripcion
				);
		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  ZONAS';
		RAISE NOTICE '------------------';
		FOR reg_zonas IN cur_zonas LOOP

			RAISE NOTICE ' PROCESANDO  %', reg_zonas.denominacion;

			INSERT INTO public.zonas(
				idtipo_zona, denominacion, descripcion, idempresa, idlocalidad, 
				fecha_creacion, fecha_borrado)
				SELECT reg_zonas.idtipo_zona, reg_zonas.denominacion, reg_zonas.descripcion, reg_zonas.idempresa, 
					reg_zonas.idlocalidad, COALESCE(reg_zonas.fecha_creacion, NOW()),
					reg_zonas.fecha_borrado
			WHERE
				NOT EXISTS (
					SELECT id FROM public.zonas WHERE denominacion = reg_zonas.denominacion
				);
		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  CALLES Y DOMICILIOS';
		RAISE NOTICE '--------------------------------';
		FOR reg_callesDomicilios IN cur_callesDomicilios LOOP

			RAISE NOTICE ' PROCESANDO  %', reg_callesDomicilios.nombre;
			RAISE NOTICE ' CLAVETOTAL  %', reg_callesDomicilios.claveTotal;
			RAISE NOTICE ' MAESTRO  %', reg_callesDomicilios.maestro;

			INSERT INTO public.catalogos
			(idmaestro, clave, descripcion, descripcion_corta, fecha_creacion, clave_total)
			SELECT reg_callesDomicilios.maestro, reg_callesDomicilios.id, reg_callesDomicilios.nombre, reg_callesDomicilios.nombre, now(), reg_callesDomicilios.claveTotal
			WHERE
				NOT EXISTS (
					SELECT id FROM public.catalogos WHERE descripcion = reg_callesDomicilios.nombre
				);

			IntIdCalle := (SELECT id FROM public.catalogos WHERE descripcion = reg_callesDomicilios.nombre);

                        RAISE NOTICE ' 			PROCESANDO  %', IntIdCalle;
                        RAISE NOTICE ' 			CALLE  %', reg_callesDomicilios.nombre;
                        RAISE NOTICE ' 			ALTURA  %', reg_callesDomicilios.altura;

			INSERT INTO public.domicilios
			(idcalle, altura, bis, letra, datos_adicionales, seleccion_manual)
			SELECT IntIdCalle, reg_callesDomicilios.altura, reg_callesDomicilios.bis, reg_callesDomicilios.letra, reg_callesDomicilios.resto, reg_callesDomicilios.seleccion_manual
			WHERE
				NOT EXISTS (
					SELECT id FROM public.domicilios WHERE idcalle = IntIdCalle AND altura = reg_callesDomicilios.altura AND letra = reg_callesDomicilios.letra AND datos_adicionales = reg_callesDomicilios.resto
				);

		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  SUCURSALES';
		RAISE NOTICE '-----------------------';
		FOR reg_sucursales IN cur_sucursales LOOP

			RAISE NOTICE '======================================================';
			RAISE NOTICE ' PROCESANDO  %', reg_sucursales.nombre;
			RAISE NOTICE ' CLAVE  %', reg_sucursales.clave;
			RAISE NOTICE ' CLAVETOTAL  %', reg_sucursales.claveTotal;
			RAISE NOTICE ' MAESTRO  %', reg_sucursales.maestro;

			IF reg_sucursales.idCalle IS NULL THEN
				RAISE NOTICE ' CARGA LA CALLE';
				reg_sucursales.clave = (SELECT lpad((select (clave::bigint+1)::text from public.catalogos where idmaestro = 25 order by id desc limit 1), largo_clave, '0') FROM public.maestros WHERE alias = 'Calles');
				reg_sucursales.claveTotal := reg_sucursales.claveTotal||reg_sucursales.clave;

				INSERT INTO public.catalogos
				(idmaestro, clave, descripcion, descripcion_corta, fecha_creacion, clave_total)
				SELECT reg_sucursales.maestro, reg_sucursales.clave, reg_sucursales.calle, reg_sucursales.calle, now(), reg_sucursales.claveTotal
				WHERE
					NOT EXISTS (
						SELECT id FROM public.catalogos WHERE descripcion = reg_sucursales.calle
					);

				reg_sucursales.idCalle := (SELECT id FROM public.catalogos WHERE descripcion = btrim(reg_sucursales.calle));

							RAISE NOTICE ' CARGA El DDOMICILIO DESPUES DE LA CALLE';
							RAISE NOTICE ' 			PROCESANDO  %', IntIdCalle;
							RAISE NOTICE ' 			CALLE  %', reg_sucursales.calle;
							RAISE NOTICE ' 			ALTURA  %', reg_sucursales.altura;

				INSERT INTO public.domicilios
				(idcalle, altura, seleccion_manual)
				SELECT IntIdCalle, reg_sucursales.altura, false
				WHERE
					NOT EXISTS (
						SELECT id FROM public.domicilios WHERE idcalle = IntIdCalle AND altura = reg_sucursales.altura
					);
			ELSE
				IF reg_sucursales.idAltura IS NULL THEN
							RAISE NOTICE ' CARGA SOLO El DDOMICILIO';
					INSERT INTO public.domicilios
					(idcalle, altura, seleccion_manual)
					SELECT reg_sucursales.idCalle, reg_sucursales.altura, false
					WHERE
						NOT EXISTS (
							SELECT id FROM public.domicilios WHERE idcalle = reg_sucursales.idCalle AND altura = reg_sucursales.altura
						);
				END IF;
			END IF;

			IntIdDomicilio := (select d.id
						from public.domicilios d
						inner join public.catalogos c
							on d.idcalle = c.id
						where btrim(c.descripcion) = btrim(reg_sucursales.calle)
							and d.altura = reg_sucursales.altura);

			RAISE NOTICE ' ID DE DDOMICILIO %', IntIdDomicilio;

			RAISE NOTICE ' INSERTO SUCURSAL %', reg_sucursales.nombre;
			RAISE NOTICE '-----------------------------------------------------';
			RAISE NOTICE ' DENOMINACION %', reg_sucursales.denominacion;
			RAISE NOTICE ' DOMICILIO %', IntIdDomicilio;
			RAISE NOTICE ' TELEFONO %', reg_sucursales.telefono;
			RAISE NOTICE ' FECHA %', reg_sucursales.fecha_alta;
			RAISE NOTICE ' CDIGO %', reg_sucursales.codigo_sucursal;

			INSERT INTO public.sucursales
			(nombre, denominacion, iddomicilio, telefono, fecha_alta, codigo)
			SELECT btrim(reg_sucursales.nombre)::text, btrim(reg_sucursales.denominacion)::text, IntIdDomicilio, btrim(reg_sucursales.telefono)::text, reg_sucursales.fecha_alta::date, reg_sucursales.codigo_sucursal::int
			WHERE
				NOT EXISTS (
					SELECT id FROM public.sucursales WHERE btrim(nombre) = btrim(reg_sucursales.nombre)
				);
		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  PUNTOS DE VENTA';
		RAISE NOTICE '----------------------------';
		FOR reg_puntosVentas IN cur_puntosVentas LOOP

			RAISE NOTICE ' PROCESANDO  %', reg_puntosVentas.denominacion;
			RAISE NOTICE ' SUCURSAL  %', reg_puntosVentas.idSucursal;

			INSERT INTO public.puntos_ventas(
				denominacion, cuenta_corriente, fecha_creacion, numero, idsucursal, manual)
				SELECT reg_puntosVentas.denominacion, reg_puntosVentas.cuenta_corriente,
					COALESCE(reg_puntosVentas.fecha_creacion, NOW()), reg_puntosVentas.numero,
					reg_puntosVentas.idSucursal, COALESCE(reg_puntosVentas.manual, false)
			WHERE
				NOT EXISTS (
					SELECT id FROM public.puntos_ventas WHERE denominacion = reg_puntosVentas.denominacion
				);
		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  USUARIOS';
		RAISE NOTICE '---------------------';
		FOR reg_usuarios IN cur_usuarios LOOP

			RAISE NOTICE ' PROCESANDO  %', reg_usuarios.usuario;
			RAISE NOTICE ' PERSONA  %', reg_usuarios.idPersona;

			INSERT INTO public.usuarios(
				idpersona, usuario, clave_acceso, fecha_creacion, fecha_borrado, es_super_usuario)
				SELECT reg_usuarios.idPersona, reg_usuarios.usuario, reg_usuarios.claveAcceso,
						reg_usuarios.fechaCreacion, reg_usuarios.fechaBorrado, reg_usuarios.esSuperUsuario
			WHERE
				NOT EXISTS (
					SELECT id FROM public.usuarios 
							WHERE idpersona = reg_usuarios.idPersona
				);
		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  COBRADORES';
		RAISE NOTICE '-----------------------';
		FOR reg_cobradores IN cur_cobradores LOOP

			RAISE NOTICE ' PROCESANDO  %', reg_cobradores.numeroLegajo;
			RAISE NOTICE ' PERSONA  %', reg_cobradores.idPersona;
			RAISE NOTICE ' EMPRESA  %', reg_cobradores.idEmpresa;

			INSERT INTO public.cobradores(
				idpersona, liquida_comision, observaciones, fecha_alta, fecha_baja, idempresa, motivo_baja, 
				numero_legajo, fecha_creacion, fecha_borrado, baja_definitiva)
				SELECT reg_cobradores.idPersona, reg_cobradores.liquidaComision, reg_cobradores.observaciones,
					COALESCE(reg_cobradores.fechaAlta, NOW()), reg_cobradores.fechaBaja, reg_cobradores.idEmpresa,
					reg_cobradores.motivoBaja, reg_cobradores.numeroLegajo, reg_cobradores.fechaCreacion,
					reg_cobradores.fechaBorrado, reg_cobradores.bajaDefinitiva
			WHERE
				NOT EXISTS (
					SELECT id FROM public.cobradores 
							WHERE idpersona = reg_cobradores.idPersona and idempresa = reg_cobradores.idEmpresa
				);
		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  ZONAS EMPLEADOS';
		RAISE NOTICE '----------------------------';
		FOR reg_zonasEmpleados IN cur_zonasEmpleados LOOP

			RAISE NOTICE ' PROCESANDO  %', reg_zonasEmpleados.idZona;
			RAISE NOTICE ' PERSONA  %', reg_zonasEmpleados.idCobrador;

			INSERT INTO public.zonas_empleados(
				fecha_desde, idzona, idcobrador)
				SELECT reg_zonasEmpleados.fechaDesde, reg_zonasEmpleados.idzona, reg_zonasEmpleados.idCobrador;
		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  VENDEDORES';
		RAISE NOTICE '-----------------------';
			RAISE NOTICE ' PROCESANDO  VENDEDOR OFICINA';

			INSERT INTO public.vendedores(
				idpersona, liquida_comision, recuperador, observaciones, numero_legajo, fecha_alta, idempresa, fecha_creacion)
			VALUES (
				(select id from personas where nombre_comercial = 'Ventas Oficina'), false, false,
				'Para Migracion de Planes ElPrado en CIBA', 0, NOW(),
				(SELECT id FROM catalogos where clave = '1' and idmaestro = (SELECT id FROM public.maestros WHERE alias = 'Empresas')),
				NOW()
			);

			RAISE NOTICE ' PROCESANDO  VENDEDOR CONVENIOS';

			INSERT INTO public.vendedores(
				idpersona, liquida_comision, recuperador, observaciones, numero_legajo, fecha_alta, idempresa, fecha_creacion)
			VALUES (
				(select id from personas where nombre_comercial = 'Ventas Convenios'), false, false,
				'Para Migracion de Convenios en CIBA', 0, NOW(),
				(SELECT id FROM catalogos where clave = '1' and idmaestro = (SELECT id FROM public.maestros WHERE alias = 'Empresas')),
				NOW()
			);

		FOR reg_vendedores IN cur_vendedores LOOP

			RAISE NOTICE ' PROCESANDO  %', reg_vendedores.numeroLegajo;

			INSERT INTO public.vendedores(
				idpersona, liquida_comision, recuperador, observaciones, posicion_ranking_manual, fecha_alta, 
				fecha_baja, baja_definitiva, motivo_baja, idempresa, numero_legajo, posicion_ranking_recupero, 
				fecha_creacion, fecha_borrado)
				SELECT reg_vendedores.idPersona, reg_vendedores.liquidaComision, reg_vendedores.recuperador, 
					reg_vendedores.observaciones, reg_vendedores.posicionRankingManual, reg_vendedores.fechaAlta, 
					reg_vendedores.fechaBaja, reg_vendedores.bajaDefinitiva, reg_vendedores.motivoBaja, 
					reg_vendedores.idEmpresa, reg_vendedores.numeroLegajo, reg_vendedores.posicionRankingRecupero, 
					reg_vendedores.fechaCreacion, reg_vendedores.fechaBorrado
			WHERE
				NOT EXISTS (
					SELECT id FROM public.vendedores 
							WHERE idpersona = reg_vendedores.idPersona
				);
				
		END LOOP;
				
		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  COORDINADORES';
		RAISE NOTICE '--------------------------';

		FOR reg_coordinadores IN cur_coordinadores LOOP

			RAISE NOTICE ' PROCESANDO  %', reg_coordinadores.codigoCoordinador;

			INSERT INTO public.coordinadores(
				codigo_coordinador, fecha_alta, fecha_baja, motivo_baja, idvendedor, liquida_comision)
				SELECT reg_coordinadores.codigoCoordinador, reg_coordinadores.fechaAlta, reg_coordinadores.fechaBaja, 
					reg_coordinadores.motivoBaja, reg_coordinadores.idvendedor, reg_coordinadores.liquidaComision
			WHERE
				NOT EXISTS (
					SELECT id FROM public.coordinadores 
							WHERE idvendedor = reg_coordinadores.idvendedor
				);
				
		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  EQUIPOS VENDEDORES';
		RAISE NOTICE '-------------------------------';

		FOR reg_equiposVendedores IN cur_equiposVendedores LOOP

			RAISE NOTICE ' PROCESANDO  %', reg_equiposVendedores.codigoEquipo;

			INSERT INTO public.equipos_vendedores(
				codigo_equipo, idvendedor, fecha_alta, fecha_baja, motivo_baja, liquida_comision)
				SELECT reg_equiposVendedores.codigoEquipo, reg_equiposVendedores.idVendedor, reg_equiposVendedores.fechaAlta,
					reg_equiposVendedores.fechaBaja, reg_equiposVendedores.motivoBaja, reg_equiposVendedores.liquidaComision
			WHERE
				NOT EXISTS (
					SELECT id FROM public.equipos_vendedores 
							WHERE codigo_equipo = reg_equiposVendedores.codigoEquipo
				);
				
		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  PERSONAS CONTACTOS';
		RAISE NOTICE '-------------------------------';

		FOR reg_personasContactos IN cur_personasContactos LOOP

			RAISE NOTICE ' PROCESANDO		%', reg_personasContactos.idPersona;
			RAISE NOTICE ' TIPO CONTACTO  	%', reg_personasContactos.idTipoContacto;
			RAISE NOTICE ' DATO			  	%', reg_personasContactos.valor;

			INSERT INTO public.personas_contactos(
				idpersona, idtipo_contacto, valor, activo)
				SELECT reg_personasContactos.idpersona, reg_personasContactos.idTipoContacto, 
				reg_personasContactos.valor, reg_personasContactos.activo;
				
		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  EQUIPO Y COORDINADOR POR VENDEDOR';
		RAISE NOTICE '----------------------------------------------';

		FOR reg_VendedoresPorEquipo IN cur_VendedoresPorEquipo LOOP

			RAISE NOTICE ' VENDEDOR		%', reg_VendedoresPorEquipo.idVendedor;
			RAISE NOTICE ' EQUIPO  		%', reg_VendedoresPorEquipo.idEquipo;
			RAISE NOTICE ' COORDINADOR	%', reg_VendedoresPorEquipo.idCoordinador;

			UPDATE public.vendedores
				SET idequipo = reg_VendedoresPorEquipo.idEquipo,
					idcoordinador = reg_VendedoresPorEquipo.idCoordinador
			WHERE id = reg_VendedoresPorEquipo.idVendedor;
				
		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  ENTIDADES FINANCIERAS';
		RAISE NOTICE '----------------------------------';

		FOR reg_entidadesFinancieras IN cur_entidadesFinancieras LOOP

			RAISE NOTICE ' ENTIDAD		%', reg_entidadesFinancieras.nombre;

			INSERT INTO public.entidades_financieras(
				nombre, fecha_alta)
				SELECT reg_entidadesFinancieras.nombre, reg_entidadesFinancieras.fechaAlta
			WHERE
				NOT EXISTS (
					SELECT id FROM public.entidades_financieras 
							WHERE nombre = reg_entidadesFinancieras.nombre
				);
				
		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  TARJETAS DE CREDITO';
		RAISE NOTICE '--------------------------------';

		FOR reg_tarjetasCredito IN cur_tarjetasCredito LOOP

			RAISE NOTICE ' TARJETA		%', reg_tarjetasCredito.numero_tarjeta;
			RAISE NOTICE ' EMPRESA		%', reg_tarjetasCredito.nombre;

			INSERT INTO public.tarjetas_creditos(
				numero_tarjeta, idtitular, idempresa_tarjeta)
				SELECT reg_tarjetasCredito.numero_tarjeta, reg_tarjetasCredito.idTitular, reg_tarjetasCredito.idEntidadFinanciera
			WHERE
				NOT EXISTS (
					SELECT id FROM public.tarjetas_creditos 
							WHERE numero_tarjeta::text = reg_tarjetasCredito.numero_tarjeta::text
				);
				
		END LOOP;

		RAISE NOTICE '======================================================';
		RAISE NOTICE ' PROCESANDO  CBU';
		RAISE NOTICE '----------------';

		FOR reg_Cbus IN cur_Cbus LOOP

			RAISE NOTICE ' NUMERO DE CUENTA		%', reg_Cbus.cbu;
			RAISE NOTICE ' EMPRESA		%', reg_Cbus.nombre;

			INSERT INTO public.cbu(
				numero_cuenta, idtitular)
				SELECT reg_Cbus.cbu, reg_Cbus.idTitular
			WHERE
				NOT EXISTS (
					SELECT id FROM public.cbu
							WHERE numero_cuenta::text = reg_Cbus.cbu::text
				);
				
		END LOOP;

		RETURN;

	END
$BODY$
LANGUAGE 'plpgsql';















#!/bin/bash
#
# Script de Migracion de datos de Cobol a Postgresql
#	El script obtiene las tablas del esquema Public de la base de datos Cobol 
#
#	Una vez obtenidas las tablas genera un archivo por cada tabla en el directorio /tmp/Migracion/tablas
#	cada rchivo contiene los registros de cada una de las tablas en formato CSV.
#
#	Recorre cada archivo CSV y lo importa en la Base de Datos [DB RECEPTORA] en el esquema MigracionCobol
#
#############################################################################################################
#	Conexiones a Bases de Datos Utilizadas:
#		psql "dbname=[DB RECEPTORA] host=[IP SERVIDOR] user=postgres password=[PASS USUARIO postgres] port=[PORT PostgreSql]"
#		psql "dbname=[DB COBOL] host=[IP SERVIDOR] user=postgres password=[PASS USUARIO postgres] port=[PORT PostgreSql]"
#		psql "dbname=[DB DESARROLLO] host=[IP SERVIDOR] user=postgres password=[PASS USUARIO postgres] port=[PORT PostgreSql]"
#
#	Ejecución de Query
#		-c "DROP TABLE IF EXISTS public.catalogos_old"
#
#	Escritura en Log
#		-L "/var/log/Migracion.log"  -A -b -e -E;
#               -A -b -e -E;
#               -A -b -e -E -L "/var/log/Migracion.log";
#
#############################################################################################################

rm /tmp/Migracion/tablas/public/*.csv
rm /tmp/Migracion/tablas/*.csv
rm /tmp/Migracion/tablas.log
rm /var/log/Migracion.log

echo "===========================================" >> /var/log/Migracion.log;
echo "=  INICIO DE LA MIGRACION DE DATOS COBOL  =" >> /var/log/Migracion.log;
echo "===========================================" >> /var/log/Migracion.log;
COMIENZO=$(date +"%Y-%m-%d %H:%M:%S")
START_TIME=$SECONDS
echo "COMENZADO: $COMIENZO" >> /var/log/Migracion.log;

echo "===============================" >> /var/log/Migracion.log;
echo "CREO LA BASE DE DATOS [DB RECEPTORA]" >> /var/log/Migracion.log;
echo "===============================" >> /var/log/Migracion.log;
psql "host=[IP SERVIDOR] user=postgres password=[PASS USUARIO postgres] port=[PORT PostgreSql]" -c 'DROP DATABASE IF EXISTS "[DB RECEPTORA]";'  -A -b -e -E -L "/var/log/Migracion.log";
psql "host=[IP SERVIDOR] user=postgres password=[PASS USUARIO postgres] port=[PORT PostgreSql]" -c 'CREATE DATABASE "[DB RECEPTORA]" WITH OWNER = postgres ENCODING = "UTF8" LC_COLLATE = "es_ES.UTF-8" LC_CTYPE = "es_ES.UTF-8" TABLESPACE = pg_default CONNECTION LIMIT = -1;'  -A -b -e -E -L "/var/log/Migracion.log";
psql "host=[IP SERVIDOR] user=postgres password=[PASS USUARIO postgres] port=[PORT PostgreSql]" -c 'ALTER DATABASE "[DB RECEPTORA]" OWNER TO postgres;'  -A -b -e -E -L "/var/log/Migracion.log";
psql "host=[IP SERVIDOR] user=postgres password=[PASS USUARIO postgres] port=[PORT PostgreSql]" -c '\q'  -A -b -e -E -L "/var/log/Migracion.log";

psql "dbname=[DB RECEPTORA] host=[IP SERVIDOR] user=postgres password=[PASS USUARIO postgres] port=[PORT PostgreSql]" -f '/tmp/Migracion/Creacion_DB_Migra.sql'  -A -b -e -E -L "/var/log/Migracion.log";
psql "dbname=[DB RECEPTORA] host=[IP SERVIDOR] user=postgres password=[PASS USUARIO postgres] port=[PORT PostgreSql]" -f '/tmp/Migracion/respaldo_cobol.sql'  -A -b -e -E -L "/var/log/Migracion.log";

echo "===============================" >> /var/log/Migracion.log;
echo "CREO LAS TABLAS DE USO TEMPORAL" >> /var/log/Migracion.log;
echo "===============================" >> /var/log/Migracion.log;

psql "dbname=[DB RECEPTORA] host=[IP SERVIDOR] user=postgres password=[PASS USUARIO postgres] port=[PORT PostgreSql]" -c "CREATE TABLE IF NOT EXISTS [SCHEMA].catalogo_old(id bigint NOT NULL, idmaestro bigint NOT NULL, clave character varying(100) COLLATE pg_catalog.default, descripcion character varying(200) COLLATE pg_catalog.default NOT NULL, descripcion_corta character varying(200) COLLATE pg_catalog.default NOT NULL, fecha_creacion date NOT NULL, fecha_borrado date, clave_total character varying(100) COLLATE pg_catalog.default NOT NULL, CONSTRAINT pk_catalogos_old PRIMARY KEY (id) )WITH ( OIDS = FALSE) TABLESPACE pg_default;"  -A -b -e -E -L "/var/log/Migracion.log";
psql "dbname=[DB RECEPTORA] host=[IP SERVIDOR] user=postgres password=[PASS USUARIO postgres] port=[PORT PostgreSql]" -c "ALTER TABLE [SCHEMA].catalogo_old OWNER to postgres;"  -A -b -e -E -L "/var/log/Migracion.log";

psql "dbname=[DB RECEPTORA] host=[IP SERVIDOR] user=postgres password=[PASS USUARIO postgres] port=[PORT PostgreSql]" -c "CREATE TABLE [SCHEMA].categorias_planes_old(id integer NOT NULL, denominacion character varying COLLATE pg_catalog.default NOT NULL, fecha_vigencia date, idempresa character varying(100) COLLATE pg_catalog.default, CONSTRAINT categorias_planes_old_pkey PRIMARY KEY (id)) WITH ( OIDS = FALSE) TABLESPACE pg_default;"  -A -b -e -E -L "/var/log/Migracion.log";
psql "dbname=[DB RECEPTORA] host=[IP SERVIDOR] user=postgres password=[PASS USUARIO postgres] port=[PORT PostgreSql]" -c "ALTER TABLE [SCHEMA].categorias_planes_old OWNER to postgres;"   -A -b -e -E -L "/var/log/Migracion.log";

psql "dbname=[DB DESARROLLO] host=[IP SERVIDOR] user=postgres password=[PASS USUARIO postgres] port=[PORT PostgreSql]" -c "COPY public.maestros TO '/tmp/Migracion/tablas/public/maestros.csv' DELIMITER ',' CSV HEADER;"  -A -b -e -E -L "/var/log/Migracion.log";
psql "dbname=[DB RECEPTORA] host=[IP SERVIDOR] user=postgres password=[PASS USUARIO postgres] port=[PORT PostgreSql]" -c "COPY public.maestros FROM '/tmp/Migracion/tablas/public/maestros.csv' DELIMITER ',' CSV HEADER;" -A -b -e -E -L "/var/log/Migracion.log";

echo "========================================================================" >> /var/log/Migracion.log;
echo "GENERANDO ARCHIVO TABLAS.LOG" >> /var/log/Migracion.log;
echo "========================================================================" >> /var/log/Migracion.log;

echo -e "Línea: $linea";
psql "dbname=[DB COBOL] host=[IP SERVIDOR] user=postgres password=[PASS USUARIO postgres] port=[PORT PostgreSql]" -c "COPY (select table_name FROM information_schema.tables where table_catalog = '[NOMBRE CATALOGO]' and table_schema = 'public' order by table_name) to '/tmp/Migracion/tablas.log'"  -A -b -e -E -L "/var/log/Migracion.log";

echo "========================================================================" >> /var/log/Migracion.log;
echo "ARCHIVO GENERADO EN: $(($SECONDS - $START_TIME)) SEGUNDOS" >> /var/log/Migracion.log;
echo "========================================================================" >> /var/log/Migracion.log;
while read linea; do
        echo "EXPORTANDO DATOS DE: $linea | TIEMPO DE INICIO: $COMIENZO" >> /var/log/Migracion.log;
        echo "========================================================================" >> /var/log/Migracion.log;
	psql "dbname=[DB COBOL] host=[IP SERVIDOR] user=postgres password=[PASS USUARIO postgres] port=[PORT PostgreSql]" -c "COPY public.$linea to '/tmp/Migracion/tablas/$linea.csv' DELIMITER ',' CSV HEADER;"  -A -b -e -E -L "/var/log/Migracion.log";
        echo "========================================================================" >> /var/log/Migracion.log;
	echo "DURACION SEGUNDOS: $(($SECONDS - $START_TIME))" >> /var/log/Migracion.log;
        echo "========================================================================" >> /var/log/Migracion.log;
done < '/tmp/Migracion/tablas.log'

echo "====================================================" >> /var/log/Migracion.log;
echo "CARGO REGISTROS EN LAS TABLAS DE USO TEMPORAL" >> /var/log/Migracion.log;
echo "====================================================" >> /var/log/Migracion.log;
psql "dbname=[DB DESARROLLO] host=[IP SERVIDOR] user=postgres password=[PASS USUARIO postgres] port=[PORT PostgreSql]" -c "COPY public.catalogos TO '/tmp/Migracion/tablas/public/catalogos.csv' DELIMITER ',' CSV HEADER;"  -A -b -e -E -L "/var/log/Migracion.log";
psql "dbname=[DB RECEPTORA] host=[IP SERVIDOR] user=postgres password=[PASS USUARIO postgres] port=[PORT PostgreSql]" -c "COPY [SCHEMA].catalogo_old FROM '/tmp/Migracion/tablas/public/catalogos.csv' DELIMITER ',' CSV HEADER;"   -A -b -e -E -L "/var/log/Migracion.log";

psql "dbname=[DB DESARROLLO] host=[IP SERVIDOR] user=postgres password=[PASS USUARIO postgres] port=[PORT PostgreSql]" -c "COPY (select cp.id, cp.denominacion, cp.fecha_vigencia, c.clave as idempresa from categorias_planes cp inner join catalogos c on c.idmaestro = 14 and cp.idempresa = c.id) TO '/tmp/Migracion/tablas/public/categorias_planes.csv' DELIMITER ',' CSV HEADER;"  -A -b -e -E -L "/var/log/Migracion.log";
psql "dbname=[DB RECEPTORA] host=[IP SERVIDOR] user=postgres password=[PASS USUARIO postgres] port=[PORT PostgreSql]" -c "COPY [SCHEMA].categorias_planes_old FROM '/tmp/Migracion/tablas/public/categorias_planes.csv' DELIMITER ',' CSV HEADER;"  -A -b -e -E -L "/var/log/Migracion.log";
echo "===============================================================================================" >> /var/log/Migracion.log;

for i in $(ls *.log)
do
        while read linea; do
		echo "EXPORTACION DE LOS DATOS DE LA TABLA COBOL: $linea | TIEMPO DE INICIO: $COMIENZO" >> /var/log/Migracion.log;
		echo "===============================================================================================" >> /var/log/Migracion.log;
		psql "dbname=[DB RECEPTORA] host=[IP SERVIDOR] user=postgres password=[PASS USUARIO postgres] port=[PORT PostgreSql]" -c "COPY [SCHEMA].$linea FROM '/tmp/Migracion/tablas/$linea.csv' DELIMITER ',' CSV HEADER;"  -A -b -e -E -L "/var/log/Migracion.log";
	        echo "========================================================================" >> /var/log/Migracion.log;
	        echo "DURACION SEGUNDOS: $(($SECONDS - $START_TIME))" >> /var/log/Migracion.log;
	        echo "========================================================================" >> /var/log/Migracion.log;
	done < $i
done

echo "===================================================" >> /var/log/Migracion.log;
echo "ACTUALIZO EL LARGO DE CLAVE PARA CALLES EN MAESTROS" >> /var/log/Migracion.log;
echo "===================================================" >> /var/log/Migracion.log;
psql "dbname=[DB RECEPTORA] host=[IP SERVIDOR] user=postgres password=[PASS USUARIO postgres] port=[PORT PostgreSql]" -c "update public.maestros set largo_clave = (select max(length(id::text)) from [SCHEMA].calles) where  alias = 'Calles';" -A -b -e -E -L "/var/log/Migracion.log";

echo "=============================================" >> /var/log/Migracion.log;
echo "EJECUTA FUNCION DE Migracion A ESQUEMA PUBLIC" >> /var/log/Migracion.log;
echo "=============================================" >> /var/log/Migracion.log;

psql "dbname=[DB RECEPTORA] host=[IP SERVIDOR] user=postgres password=[PASS USUARIO postgres] port=[PORT PostgreSql]" -f "/tmp/Migracion/fnproceso.sql"  -A -b -e -E -L "/var/log/Migracion.log";
psql "dbname=[DB RECEPTORA] host=[IP SERVIDOR] user=postgres password=[PASS USUARIO postgres] port=[PORT PostgreSql]" -c "SELECT [SCHEMA].proceso();"  -A -b -e -E -L "/var/log/Migracion.log";

echo "========================================================================" >> /var/log/Migracion.log;
echo "DURACION SEGUNDOS: $(($SECONDS - $START_TIME))" >> /var/log/Migracion.log;
echo "========================================================================" >> /var/log/Migracion.log;

FIN=$(date +"%Y-%m-%d %H:%M:%S");

echo "TERMINADO: $FIN" >> /var/log/Migracion.log;
echo "SEGUNDOS: $(($SECONDS - $START_TIME))" >> /var/log/Migracion.log;
echo "========================================================================" >> /var/log/Migracion.log;
